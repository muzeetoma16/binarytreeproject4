module com.example.binarytreeproject {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.binarytreeproject to javafx.fxml;
    exports com.example.binarytreeproject;
}