package com.example.binarytreeproject;

public class SBinaryTree {

    public Node root;

    public void addNode(String value) {
        root = add(root, value);
    }

    private Node add(Node current, String value) {

        if (current == null) {
            return new Node(value);
        }

        if (value.compareToIgnoreCase(current.value) > 0) {
            current.left = add(current.left, value);
        } else if (value.compareToIgnoreCase(current.value) < 0) {
            current.right = add(current.right, value);
        }

        return current;
    }

    public boolean isEmpty() {
        return root == null;
    }

    public int getSize() {
        return getSize(root);
    }

    private int getSize(Node current) {
        return current == null ? 0 : getSize(current.left) + 1 + getSize(current.right);
    }

    public boolean containsNode(String value) {
        return containsNode(root, value);
    }

    private boolean containsNode(Node current, String value) {
        if (current == null) {
            return false;
        }

        if (value.equalsIgnoreCase(current.value)) {
            return true;
        }

        return value.compareToIgnoreCase(current.value) > 0
                ? containsNode(current.left, value)
                : containsNode(current.right, value);
    }

    public void delete(String value) {
        root = delete(root, value);
    }

    private Node delete(Node current, String value) {
        if (current == null) {
            return null;
        }

        if (value.equalsIgnoreCase(current.value)) {
            // Case 1: no children
            if (current.left == null && current.right == null) {
                return null;
            }

            // Case 2: only 1 child
            if (current.right == null) {
                return current.left;
            }

            if (current.left == null) {
                return current.right;
            }

            // Case 3: 2 children
            String smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = delete(current.right, smallestValue);
            return current;
        }
        if (value.compareToIgnoreCase(current.value) > 0) {
            current.left = delete(current.left, value);
            return current;
        }

        current.right = delete(current.right, value);
        return current;
    }

    private String findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    public String traverseInOrder(Node node, StringBuilder stringBuilder) {
        if (node != null) {
            traverseInOrder(node.left,stringBuilder);
            visit(node.value, stringBuilder);
            traverseInOrder(node.right,stringBuilder);
        }

        return stringBuilder.toString();
    }

    public String traversePreOrder(Node node,StringBuilder stringBuilder) {
        if (node != null) {
            visit(node.value,stringBuilder);
            traversePreOrder(node.left,stringBuilder);
            traversePreOrder(node.right,stringBuilder);
        }

        return stringBuilder.toString();
    }

    public String traversePostOrder(Node node,StringBuilder stringBuilder) {
        if (node != null) {
            traversePostOrder(node.left,stringBuilder);
            traversePostOrder(node.right,stringBuilder);
            visit(node.value,stringBuilder);
        }
        return stringBuilder.toString();
    }

    private String visit(String value, StringBuilder stringBuilder) {
        stringBuilder.append(" ").append(value);

        return stringBuilder.toString();
    }


    public static class Node {
        String value;
        Node left;
        Node right;

        Node(String value) {
            this.value = value;
            right = null;
            left = null;
        }

        public String getValue() {
            return value;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }
    }



}
