package com.example.binarytreeproject;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FormController {

    @FXML
    public TextField addNewItemInput;
    @FXML
    private TextField searchItemInput;
    @FXML
    private TextField deleteItemInput;

    private SBinaryTree sBinaryTree = new SBinaryTree();

    final StringBuilder inOrderStringBuilder = new StringBuilder();
    final StringBuilder preOrderStringBuilder = new StringBuilder();
    final StringBuilder postOrderStringBuilder = new StringBuilder();
    private Alert alert;

    @FXML
    protected void addNewItemButtonClick() {

        if(addNewItemInput.getText().isEmpty()){
            alert = new Alert(Alert.AlertType.ERROR,"Please Enter an Item");
            alert.show();
        }else {

            sBinaryTree.addNode(addNewItemInput.getText());
            addNewItemInput.clear();
            alert = new Alert(Alert.AlertType.CONFIRMATION, "Item added !");
            alert.show();
        }
    }

    @FXML
    public void searchItemClick() {
    }

    @FXML
    public void deleteItemClick() {
    }

    @FXML
    protected void viewBinaryTreeClick() {
        SBinaryTreePrinter sBinaryTreePrinter = new SBinaryTreePrinter(sBinaryTree.root);
        if(sBinaryTree.isEmpty()){
            alert = new Alert(Alert.AlertType.WARNING, "The Binary Tee is empty");
            alert.show();
        }else {
            alert = new Alert(Alert.AlertType.INFORMATION, sBinaryTreePrinter.getTree());
            alert.showAndWait();
        }
    }


    public void inOrderTraversalClick() {
        if(sBinaryTree.isEmpty()){
            alert = new Alert(Alert.AlertType.WARNING, "The Binary Tee is empty");
            alert.show();
        }else {
            inOrderStringBuilder.setLength(0);
            alert = new Alert(Alert.AlertType.INFORMATION,
                    sBinaryTree.traverseInOrder(sBinaryTree.root,inOrderStringBuilder));
            alert.showAndWait();
        }
    }

    public void preOrderTraversalClick() {
        if(sBinaryTree.isEmpty()){
            alert = new Alert(Alert.AlertType.WARNING, "The Binary Tee is empty");
            alert.show();
        }else {
            preOrderStringBuilder.setLength(0);
            alert = new Alert(Alert.AlertType.INFORMATION,
                    sBinaryTree.traversePreOrder(sBinaryTree.root,preOrderStringBuilder));
            alert.showAndWait();
        }
    }

    public void postOrderTraversalClick() {
        if(sBinaryTree.isEmpty()){
            alert = new Alert(Alert.AlertType.WARNING, "The Binary Tee is empty");
            alert.show();
        }else {
            postOrderStringBuilder.setLength(0);
            alert = new Alert(Alert.AlertType.INFORMATION,
                    sBinaryTree.traversePostOrder(sBinaryTree.root,postOrderStringBuilder));
            alert.showAndWait();
        }
    }
}